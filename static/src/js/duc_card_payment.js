function disableButton() {
    $("body").block({overlayCSS: {backgroundColor: "#000", opacity: 0, zIndex: 1050}, message: false});
    $('input[name="pay_btn"]').attr('disabled', true);
    $('input[name="pay_btn"]').children('.fa-lock').removeClass('fa-lock');
    $('input[name="pay_btn"]').prepend('<span class="o_loader"><i class="fa fa-refresh fa-spin"></i>&nbsp;</span>');
 };

 function enableButton() {
    $('body').unblock();
    $('input[name="pay_btn"]').attr('disabled', false);
    $('input[name="pay_btn"]').children('.fa').addClass('fa-lock');
    $('input[name="pay_btn"]').find('span.o_loader').remove();
 };


 function check_card_and_pay(event){
   $('span[name="error_message"]').text('');
   var valid_form = validate_pay_form();
   if (valid_form){
      disableButton();
      var msg = "We are processing your payment, please wait ...";
      $.blockUI({
         'message': '<h2 class="text-white">' +
                    '<p><img src="/duc_card_payment/static/src/img/duc_logo.png"/></p>' +
                    '<p><img src="/web/static/src/img/spin.png" class="fa-pulse"/></p>' +
                    '    <br />' + //msg +
                    '</h2>'
      });
      pay();
   }
 };


 function pay(){
    payload = {
         creditCardNumber: $('input[name="creditCardNumber"]').val(),
         creditCardHolderName: $('input[name="creditCardHolderName"]').val(),
         expiryDate: $('input[name="expiryDate"]').val(),
         cvc: $('input[name="cvc"]').val(),
         order_id: $('input[name="order_id"]').val(),
         payment_id: $('input[name="payment_id"]').val(),
         currency: $('input[name="currency"]').val(),
         reference: $('input[name="reference"]').val(),
         address: $('input[name="address"]').val(),
         zipcode: $('input[name="zipcode"]').val(),
         email: $('input[name="email"]').val(),
         phone: $('input[name="phone"]').val(),
         partner_id: $('input[name="partner_id"]').val(),
      };

      $.ajax({
           type: 'post',
           dataType: 'json',
           contentType: "application/json; charset=utf-8",
           url: '/payment/ducapi2/pay',
           data: JSON.stringify({'jsonrpc': "2.0", 'method': "call", "params": payload}),
           success: function (data) {
              if (data.hasOwnProperty('error') && data.error.hasOwnProperty('message')) {
                $('span[name="error_message"]').text(data.error.message);
                enableButton();
                $.unblockUI();
              }else if (data.hasOwnProperty('result') && data.result.hasOwnProperty('error')){
                $('span[name="error_message"]').text(data.result.message);
                enableButton();
                $.unblockUI();
              }else if (data.hasOwnProperty('result') && data.result.hasOwnProperty('status')){
                $('input[name="status"]').val(data.result.status);
                $('form[name="card_form"]').submit();
                //$.unblockUI();
              }else{
                 $('span[name="error_message"]').text('Error desconocido');
                 enableButton();
                 $.unblockUI();
              }
           }
       });
 };

 function validate_pay_form(){
     var valid = true;
     if ($('input[name="email"]').val() == ''){
        $('input[name="email"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="email"]').css('border-color', '#ced4da');
     }

     if ($('input[name="phone"]').val() == ''){
        $('input[name="phone"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="phone"]').css('border-color', '#ced4da');
     }

     if ($('input[name="creditCardNumber"]').val() == ''){
        $('input[name="creditCardNumber"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="creditCardNumber"]').css('border-color', '#ced4da');
     }

     if ($('input[name="creditCardHolderName"]').val() == ''){
        $('input[name="creditCardHolderName"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="creditCardHolderName"]').css('border-color', '#ced4da');
     }

     if ($('input[name="expiryDate"]').val() == ''){
        $('input[name="expiryDate"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="expiryDate"]').css('border-color', '#ced4da');
     }

     if ($('input[name="cvc"]').val() == ''){
        $('input[name="cvc"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="cvc"]').css('border-color', '#ced4da');
     }

     if ($('input[name="address"]').val() == ''){
        $('input[name="address"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="address"]').css('border-color', '#ced4da');
     }

     if ($('input[name="zipcode"]').val() == ''){
        $('input[name="zipcode"]').css('border-color', 'red');
        valid = false;
     }else{
        $('input[name="zipcode"]').css('border-color', '#ced4da');
     }

     return valid;
 }

 /**********************************************************************************/

 function formatString(e) {
   var inputChar = String.fromCharCode(event.keyCode);
   var code = event.keyCode;
   var allowedKeys = [8];
   if (allowedKeys.indexOf(code) !== -1) {
     return;
   }

   event.target.value = event.target.value.replace(
     /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
   ).replace(
     /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
   ).replace(
     /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
   ).replace(
     /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
   ).replace(
     /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
   ).replace(
     /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
   ).replace(
     /\/\//g, '/' // Prevent entering more than 1 `/`
   );
 }

 /***********************************************************************************/
 function validarNumero(e) {
   tecla = (document.all) ? e.keyCode : e.which;
   if (tecla==8) return true;
   patron =/[0-9]/;
   te = String.fromCharCode(tecla);
   return patron.test(te);
}

