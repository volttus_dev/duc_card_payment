# -*- coding: utf-8 -*-

import json
import logging
import pprint
import werkzeug
import requests
from datetime import datetime
from time import time
import asyncio

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class DUCApi2Controller(http.Controller):
    @http.route([
        '/payment/ducapi2/test/',
    ], type='http', auth='public', website=True, csrf=False)
    def ducapi_test(self, **post):
        _logger.info('test')
        return "<h1>This is a test</h1>"

    def saveJsonToFile(self, json):
        jsonFile = open("/home/merlinux/data.json", "w")
        jsonFile.write(json)
        jsonFile.close()

    def checkPaymentDoneStatus(self, nroTransaction):
        transaction = request.env['payment.transaction'].sudo().search(
            [('reference', '=', nroTransaction), ('state', '=', 'done')], limit=1)
        return transaction

    _return_url = '/payment/ducapi2/return/'

    @http.route([
        '/payment/ducapi2/return',
    ], type='json', auth='public', methods=['POST'], csrf=False)
    def ducapi_return(self, **post):
        try:
            data = json.loads(request.httprequest.data)
            # self.saveJsonToFile(json.dumps(data))
            # _logger.info('Beginning DUCApi form_feedback with post data %s', pprint.pformat(data))  # debug

            if 'transaction' in data:
                transaction = data.get('transaction')

                if 'transactionStatus' in transaction:
                    if transaction.get('transactionStatus') == 'accepted':  # not in ['CANCELLED', 'REJECTED']:
                        is_done = self.checkPaymentDoneStatus(transaction.get('externalID'))
                        if ('transactionID' in transaction) and ('externalID' in transaction) and not is_done:
                            payload = {
                                "reference": transaction.get("externalID"),
                                "amount": transaction.get("transactionAmount"),
                                "currency": transaction.get("currency"),
                                "status": "done",
                            }
                            request.env['payment.transaction'].sudo().form_feedback(payload, 'ducapi')

                            reference = transaction.get('externalID').split("-")
                            order_name = reference[0]
                            order = request.env['sale.order'].sudo().search([('name', '=', order_name)], limit=1)
                            if order:
                                order.sudo().write({'duc_transaction_id': transaction.get('transactionID')})

                            return werkzeug.utils.redirect('/payment/process')
        except Exception as ex:
            _logger.info('DUCApi Return EndPoint: ' + str(ex))

    _accept_url = '/payment/ducapi2/feedback'

    @http.route([
        '/payment/ducapi2/feedback',
    ], type='http', auth='public', methods=['POST'], website=True, csrf=False)
    def ducapi2_form_feedback(self, **post):
        order = request.website.sale_get_order(force_create=1)
        partner = request.env['res.partner'].sudo().search([('id', '=', order.partner_id.id)], limit=1)
        values = {
            'amount_untaxed': '${:,.2f}'.format(order.amount_untaxed),
            'amount_tax': '${:,.2f}'.format(order.amount_tax),
            'amount_delivery': '${:,.2f}'.format(order.amount_delivery),
            'amount_total': '${:,.2f}'.format(order.amount_total),
            'order_id': order.id,
            'payment_id': post['payment_id'],
            'currency': post['currency'],
            'reference': post['reference'],
            'address': partner.street,
            'zipcode': partner.zip,
            'email': partner.email or '',
            'phone': partner.phone or '',
            'partner_id': partner.id,
        }
        return request.render('duc_card_payment.credit_card_info', values)

    _ducapi_pay_url = '/payment/ducapi2/pay'

    @http.route([
        '/payment/ducapi2/pay',
    ], type='json', auth='public', methods=['POST'], website=True, csrf=False)
    def ducapi2_form_pay(self, **post):
        try:
            # Save partner data
            phone = post["phone"]
            email = post["email"]
            partner_id = post["partner_id"]

            partner = request.env['res.partner'].sudo().search([('id', '=', partner_id)], limit=1)

            if partner:
                partner.phone = phone
                partner.email = email

            order_id = post['order_id']
            order = request.env['sale.order'].sudo().search([('id', '=', order_id)], limit=1)
            dt = datetime.now()

            if order.duc_transaction_id:
                return {'error': 'DUC Error',
                        'message': 'La orden ya ha sido pagada usando la transaccion ' + order.duc_transaction_id + ' de DUC'}

            payment = request.env['payment.acquirer'].sudo().search([('id', '=', post['payment_id'])], limit=1)

            headers = {"x-api-key": payment.ducapi2_api_key}
            data = {"phone": payment.ducapi2_merchant_phone, "password": payment.ducapi2_merchant_password}
            url = payment.ducapi2_api_url + "/auth/login"
            resp = requests.post(url, data, headers=headers)

            res_json = json.loads(resp.text)
            if 'error' in res_json:
                return {'error': res_json['error'], 'message': res_json['message']}

            ducapi_token = json.loads(resp.text)["accessToken"]
            headers = {
                "Authorization": "Bearer " + ducapi_token,
                "x-api-key": payment.ducapi2_api_key,
                "Content-Type": "application/json",
            }

            data = {
                "externalID": post['reference'],
                "amount": order.amount_total,  # 1,
                "currency": post['currency'],  # "CAD",
                "creditCardNumber": post["creditCardNumber"],  # "4000056655665556",
                "creditCardHolderName": post["creditCardHolderName"],  # "Test API",
                "expiryDate": post["expiryDate"],  # "05/22",
                "cvc": post["cvc"],  # "123",
                "avs_street_name": post["address"],  # order.partner_id.street or '', # "Main Street",
                # "avs_street_number": "508",
                "avs_zipcode": post["zipcode"],  # order.partner_id.zip or '', # "80100",
                "email": order.partner_id.email or '',  # "popo@gmail.com",
                "phone": order.partner_id.phone or '',  # "+5354522411",
                "location": {
                    "latitude": order.partner_id.partner_latitude,  # "-76.25804853625596",
                    "longitude": order.partner_id.partner_longitude,  # "20.888864980079234",
                    "timestamp": datetime.timestamp(dt),  # "1635185398"
                }
            }

            url = payment.ducapi2_api_url + payment.ducapi2_payment_page
            resp = requests.post(url, data=json.dumps(data), headers=headers)
            res_json = json.loads(resp.text)
            if 'error' in res_json:
                return {'error': res_json['error'], 'message': res_json['message']}

            if ('data' in res_json) and ('status' in res_json['data']):
                if (res_json['data']['status'] == 200) or (res_json['data']['status'] == 201):
                    payload = {
                        "status": 'pending',
                    }

                    # data = {
                    #     "reference": post['reference'],
                    #     "amount": order.amount_total,
                    #     "currency": post['currency'],
                    #     "status": 'pending',
                    # }
                    # request.env['payment.transaction'].sudo().form_feedback(data, 'ducapi2')
                    # order.write({'duc_transaction_id': res_json['transactionID']})
                else:
                    payload = {
                        'error': 'DUC',
                        'message': res_json['payload']  # 'Error desconocido',
                    }
            else:
                payload = {
                    'error': 'DUC',
                    'message': 'Error desconocido',
                }

            return payload
        except:
            payload = {
                'error': 'DUC',
                'message': 'Ha ocurrido un error en su proveedor de pago',
            }
            return payload

    _redirect_url = '/payment/ducapi2/redirect'

    @http.route([
        '/payment/ducapi2/redirect',
    ], type='http', auth='public', methods=['POST'], website=True, csrf=False)
    def ducapi2_redirect(self, **post):
        return werkzeug.utils.redirect('/payment/process')
