# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* payment_ducapi
# 
# Translators:
# Yoshi Tashiro <tashiro@roomsfor.hk>, 2019
# Norimichi Sugimoto <norimichi.sugimoto@tls-ltd.co.jp>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-26 08:16+0000\n"
"PO-Revision-Date: 2019-08-26 09:12+0000\n"
"Last-Translator: Norimichi Sugimoto <norimichi.sugimoto@tls-ltd.co.jp>, 2019\n"
"Language-Team: Japanese (https://www.transifex.com/odoo/teams/41243/ja/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ja\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: payment_ducapi
#: code:addons/payment_ducapi/models/payment.py:0
#, python-format
msgid "; multiple order found"
msgstr "; 複数の注文が見つかりました"

#. module: payment_ducapi
#: code:addons/payment_ducapi/models/payment.py:0
#, python-format
msgid "; no order found"
msgstr "; 注文が見つかりません"

#. module: payment_ducapi
#: model:ir.model.fields.selection,name:payment_ducapi.selection__payment_acquirer__provider__ducapi
msgid "DUCApi"
msgstr "DUCApi"

#. module: payment_ducapi
#: code:addons/payment_ducapi/models/payment.py:0
#, python-format
msgid "DUCApi: feedback error"
msgstr ""

#. module: payment_ducapi
#: code:addons/payment_ducapi/models/payment.py:0
#, python-format
msgid "DUCApi: invalid merchantSig, received %s, computed %s"
msgstr ""

#. module: payment_ducapi
#: code:addons/payment_ducapi/models/payment.py:0
#, python-format
msgid "DUCApi: received data for reference %s"
msgstr ""

#. module: payment_ducapi
#: code:addons/payment_ducapi/models/payment.py:0
#, python-format
msgid ""
"DUCApi: received data with missing reference (%s) or missing pspReference "
"(%s)"
msgstr ""

#. module: payment_ducapi
#: model_terms:ir.ui.view,arch_db:payment_ducapi.acquirer_form_ducapi
msgid "How to configure your DUCApi account?"
msgstr ""

#. module: payment_ducapi
#: model:ir.model.fields,field_description:payment_ducapi.field_payment_acquirer__ducapi_merchant_account
msgid "DUC Api Key"
msgstr ""

#. module: payment_ducapi
#: model:ir.model,name:payment_ducapi.model_payment_acquirer
msgid "Payment Acquirer"
msgstr "決済サービス"

#. module: payment_ducapi
#: model:ir.model,name:payment_ducapi.model_payment_transaction
msgid "Payment Transaction"
msgstr "決済トランザクション"

#. module: payment_ducapi
#: model:ir.model.fields,field_description:payment_ducapi.field_payment_acquirer__provider
msgid "Provider"
msgstr "プロバイダ"

#. module: payment_ducapi
#: model:ir.model.fields,field_description:payment_ducapi.field_payment_acquirer__ducapi_skin_code
msgid "Skin Code"
msgstr ""

#. module: payment_ducapi
#: model:ir.model.fields,field_description:payment_ducapi.field_payment_acquirer__ducapi_skin_hmac_key
msgid "Skin HMAC Key"
msgstr ""
