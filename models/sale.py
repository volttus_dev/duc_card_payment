from odoo import api, fields, models


class VASaleOrder(models.Model):
    _inherit = "sale.order"

    duc_transaction_id = fields.Char('DUC Transaction ID', groups='base.group_user', required=False)

