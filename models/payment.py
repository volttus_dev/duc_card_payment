# coding: utf-8
import requests
import json
import logging
import pprint
from odoo.tools.float_utils import float_compare


from werkzeug import urls

from odoo import api, fields, models, tools, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.addons.payment_ducapi.controllers.main import  DUCApiController
from odoo.tools.pycompat import to_text

_logger = logging.getLogger(__name__)



class VAAcquirerDUCApi(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('ducapi2', 'DUC Card Payment')])

    ducapi2_api_url = fields.Char('API URL', default="https://backend.ducapp.net/api", required_if_provider='ducapi2',
                                 groups='base.group_user')
    ducapi2_api_key = fields.Char('API Key', default="test.2de91c0c-60c2-5d4c-ad8c-c1c78de99b55", required_if_provider='ducapi2', groups='base.group_user')
    ducapi2_mercant_name = fields.Char('DUC Merchant name', default="lazaro.aguila@gmail.com", required_if_provider='ducapi2', groups='base.group_user')
    ducapi2_merchant_password = fields.Char('DUC Merchant password', default="XXXXXX", required_if_provider='ducapi2', groups='base.group_user')
    ducapi2_merchant_phone = fields.Char('DUC Merchant phone', default="+5353936403",
                                           required_if_provider='ducapi2', groups='base.group_user')
    # ducapi2_merchant_wallet_address = fields.Char('DUC Wallet address', default="0x065Cc26Ba957334CAd2b68B9FBbAe3843E27f75f", required_if_provider='ducapi2', groups='base.group_user')

    ducapi2_payment_page = fields.Char('DUC Payment page Url', default='/private/transactions/token/buy', required_if_provider='ducapi2',
                                      groups='base.group_user')
    ducapi2_payment_status_page = fields.Char('DUC Payment page Status Url', default='/private/transactions/',
                                       required_if_provider='ducapi2',
                                       groups='base.group_user')
    # ducapi2_payment_timeout = fields.Char('DUC Payment Time Out', groups='base.group_user', required=False)
    ducapi2_token = fields.Char('DUC Token', groups='base.group_user', required=False)


    def ducapi2_form_generate_values(self, values):
        self.ensure_one()

        # "amount": '%d' % (paymentAmount / 100),

        values.update({
           "payment_id": self.id,
           "reference": values['reference'],
           "currency": values['currency'].name,
           "status": "pending",
        })

        return values

    def ducapi2_get_form_action_url(self):
        return '/payment/ducapi2/feedback'


class TxVADUCApi(models.Model):
    _inherit = 'payment.transaction'

    @api.model
    def _ducapi2_form_get_tx_from_data(self, data):
        reference = data.get('reference')
        tx = self.search([('reference', '=', reference)])

        if not tx or len(tx) > 1:
            error_msg = _('received data for reference %s') % (pprint.pformat(reference))
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        return tx

    def _ducapi2_form_get_invalid_parameters(self, data):
        invalid_parameters = []

        if float_compare(float(data.get('amount') or '0.0'), self.amount, 2) != 0:
            invalid_parameters.append(('amount', data.get('amount'), '%.2f' % self.amount))
        if data.get('currency') != self.currency_id.name:
            invalid_parameters.append(('currency', data.get('currency'), self.currency_id.name))

        return invalid_parameters

    def _ducapi2_form_validate(self, data):
        # if data.get('status') == 'APPROVED':
        #     self._set_transaction_done()
        #     return True

        if data.get('status') == 'pending':
            self._set_transaction_pending()
            return True

        # if data.get('status') == 'rejected':
        #     self._set_transaction_cancel()
        #     return False
        # else:
        #     error = data.get("failure_message") or tree.get('error', {}).get('message')
        #     self._set_transaction_error(error)
        #     return False

