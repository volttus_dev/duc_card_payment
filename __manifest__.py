# -*- coding: utf-8 -*-

{
    'name': 'DUC Card Payment',
    'category': 'Accounting/Payment',
    'summary': 'Payment Acquirer: DUC Card Payment',
    'version': '1.0',
    'description': """DUC Card Payment""",
    'depends': ['payment', 'sale'],
    'data': [
        'views/payment_views.xml',
        'views/payment_ducapi_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'installable': True,
    'uninstall_hook': 'uninstall_hook',
}
